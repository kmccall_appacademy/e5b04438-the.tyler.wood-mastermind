class Code
  attr_reader :pegs

  PEGS = {
    r: "red",
    g: "green",
    y: "yellow",
    b: "blue",
    o: "orange",
    p: "purple"
  }

  def initialize(user_pegs)
    user_pegs.map!(&:downcase).map!(&:to_sym)

    if user_pegs.any? { |peg| !PEGS.keys.include?(peg) }
      raise "error invalid colors #{pegs}"
    end

    @pegs = user_pegs
  end

  def [](i)
    @pegs[i]
  end

  def exact_matches(code)
    matches = []
    self.pegs.each_with_index do |peg, index|
      matches.push(index) if peg == code[index]
    end
    matches.count
  end

  def near_matches(code)
    nmatches = 0
    compare_pegs = code.pegs
    self.pegs.each_with_index do |peg, index|
      if peg != code[index] # if not an exact match

        compare_pegs.each_with_index do |peg2, index2| # check against all other pegs
          if peg == peg2 # if peg matches
            if self[index2] != peg2
              nmatches += 1 # increment nmatches
              compare_pegs[index2] = :X # destroy that item for further comparison
              break
            end
          end
        end

      end
    end
    nmatches
  end

  def ==(code)
    if self.exact_matches(code) == 4
      true
    else
      false
    end
  end

  def self.parse(input)
    Code.new(input.chars)
  end

  def self.random
    string = []
    4.times do
      string.push(PEGS.keys[rand(5)].to_s)
    end
    Code.new(string)
  end

end

class Game
  attr_reader :secret_code
  attr_accessor :guess

  def initialize(code = Code.random)
    @secret_code = code
  end

  def get_guess
    @guess = Code.parse(gets.chomp)
  end

  def display_matches(code)
    puts "exact matches: #{code.exact_matches(@secret_code)}\n
     near matches: #{code.near_matches(@secret_code)}"
  end

end
